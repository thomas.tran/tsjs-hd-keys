/**
 * Copyright 2019 NEM
 *
 * Licensed under the BSD 2-Clause License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://opensource.org/licenses/BSD-2-Clause
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
import {expect} from 'chai';
import {
    Account,
    NetworkType,
    PublicAccount,
} from 'tsjs-xpx-chain-sdk';

// internal dependencies
import {
    CurveAlgorithm,
    ExtendedKey,
    KeyEncoding,
    Network,
    NodeEd25519,
    NodeInterface,
    Wallet,
} from '../index';
import { MACType } from '../src/MACType';

describe('Wallet -->', () => {

    const masterSeed = '000102030405060708090a0b0c0d0e0f';
    const chainCode  = '90046a93de5380a72b5e45010748567d5ea02bbf6522f979e05c0d8d8ca9fffb';

    // m
    const masterPriv = '2b4be7f19ee27bbf30c667b642d5f4aa69fd169872f8fc3059c08ebae2eb19e7';
    const masterPub  = '398d57dda0faae646097435e648a2c10f0f367b67e9a1e99a3d9170948d85750';

    // m/44'/43'/0'/0'/0'
    const defaultPriv = '4ce1c399f5f72acf16e7231a406f6e8284033f686d565100fed376960ea8c871';
    const defaultPub = '81357c2d65be8b12aa2506fc315b6e1e2b6ab847727d460c3c7d13755ab62395';

    // m/44'/43'/1'/0'/0'
    const secondPriv = '1b05cb9db696df7216bd6a551c0e2b441234a59b23d785f4c803a41d64ce4d69';
    const secondPub = 'af0868a6edcc3e768854fae2e3534bed3cce222893b16fdc5d780f82cd8990dd';

    describe('constructor should', () => {

        it('take extended key and set read-only to false when non-neutered', () => {
            const xkey = ExtendedKey.createFromSeed(masterSeed, Network.CATAPULT);
            const wallet = new Wallet(xkey);

            expect(wallet.isReadOnly()).to.be.equal(false);
        });

        it('take extended key and set read-only to true when neutered', () => {
            const xkey = ExtendedKey.createFromSeed(masterSeed, Network.CATAPULT);
            const xpub = xkey.getPublicNode();
            const wallet = new Wallet(xpub);

            expect(wallet.isReadOnly()).to.be.equal(true);
        });

        it('take extended key to create wallet and get correct private key', () => {
            const xkey = ExtendedKey.createFromSeed(masterSeed, Network.CATAPULT);
            const wallet = new Wallet(xkey);
            const account = wallet.getAccount();

            expect(account.privateKey.toLowerCase()).to.be.equal(masterPriv);
        });
    });

    describe('getAccount() should', () => {

        it('throw when wallet initialized with extended public key', () => {
            const xkey = ExtendedKey.createFromSeed(masterSeed, Network.CATAPULT);
            const xpub = xkey.getPublicNode();
            const wallet = new Wallet(xpub);
            expect((function() {
                wallet.getAccount();
            })).to.throw('Missing private key, please use method getPublicAccount().');
        });

        it('throw when extended key network does not match requested networkType', () => {
            const xkey = ExtendedKey.createFromSeed(masterSeed, Network.CATAPULT);
            const wallet = new Wallet(xkey);

            expect((function() {
                wallet.getAccount(NetworkType.MAIN_NET);
            })).to.throw('Inconsistent networkType.');
        });

        it('get catapult compatible private key / public key pair (keypair)', () => {
            const xkey = ExtendedKey.createFromSeed(masterSeed, Network.CATAPULT);
            const wallet = new Wallet(xkey);
            const account = wallet.getAccount();

            expect(account.privateKey.toLowerCase()).to.be.equal(masterPriv);
            expect(account.publicKey.toLowerCase()).to.be.equal(masterPub);
        });
    });

    describe('getChildAccount() should', () => {

        it('throw when wallet initialized with extended public key', () => {
            const xkey = ExtendedKey.createFromSeed(masterSeed, Network.CATAPULT);
            const xpub = xkey.getPublicNode();
            const wallet = new Wallet(xpub);

            expect((function() {
                wallet.getChildAccount();
            })).to.throw('Missing private key, please use method getChildPublicAccount().');
        });

        it('throw when extended key network does not match requested networkType', () => {
            const xkey = ExtendedKey.createFromSeed(masterSeed, Network.CATAPULT);
            const wallet = new Wallet(xkey);

            expect((function() {
                wallet.getChildAccount('m/44\'/43\'/0\'/0\'/0\'', NetworkType.MAIN_NET);
            })).to.throw('Inconsistent networkType.');
        });

        it('derive default account when given no path', () => {
            const xkey = ExtendedKey.createFromSeed(masterSeed, Network.CATAPULT);
            const wallet = new Wallet(xkey);
            const account = wallet.getChildAccount();

            expect(account.privateKey.toLowerCase()).to.be.equal(defaultPriv);
            expect(account.publicKey.toLowerCase()).to.be.equal(defaultPub);
        });

        it('derive second account when given path m/44\'/43\'/1\'/0\'/0\'', () => {
            const xkey = ExtendedKey.createFromSeed(masterSeed, Network.CATAPULT);
            const wallet = new Wallet(xkey);
            const account = wallet.getChildAccount('m/44\'/43\'/1\'/0\'/0\'');

            expect(account.privateKey.toLowerCase()).to.be.equal(secondPriv);
            expect(account.publicKey.toLowerCase()).to.be.equal(secondPub);
        });
    });

    describe('getPublicAccount() should', () => {

        it('throw when extended key network does not match requested networkType', () => {
            const xkey = ExtendedKey.createFromSeed(masterSeed, Network.CATAPULT);
            const wallet = new Wallet(xkey);

            expect((function() {
                const account = wallet.getPublicAccount(NetworkType.MAIN_NET);
            })).to.throw('Inconsistent networkType.');
        });

        it('get catapult compatible read-only account given extended private key', () => {
            const xkey = ExtendedKey.createFromSeed(masterSeed, Network.CATAPULT);
            const wallet = new Wallet(xkey);
            const account = wallet.getPublicAccount();

            expect(account).to.be.instanceof(PublicAccount);
            expect(account.publicKey.toLowerCase()).to.be.equal(masterPub);
        });

        it('get catapult compatible read-only account given extended public key', () => {
            const xkey = ExtendedKey.createFromSeed(masterSeed, Network.CATAPULT);
            const xpub = xkey.getPublicNode();
            const wallet = new Wallet(xpub);
            const account = wallet.getPublicAccount();

            expect(account).to.be.instanceof(PublicAccount);
            expect(account.publicKey.toLowerCase()).to.be.equal(masterPub);
        });

    });

    describe('getChildPublicAccount() should', () => {

        it('throw when extended key network does not match requested networkType', () => {
            const xkey = ExtendedKey.createFromSeed(masterSeed, Network.CATAPULT);
            const wallet = new Wallet(xkey);

            expect((function() {
                wallet.getChildPublicAccount('m/44\'/43\'/0\'/0\'/0\'', NetworkType.MAIN_NET);
            })).to.throw('Inconsistent networkType.');
        });

        it('derive default account when given no path', () => {
            const xkey = ExtendedKey.createFromSeed(masterSeed, Network.CATAPULT);
            const wallet = new Wallet(xkey);
            const account = wallet.getChildPublicAccount();

            expect(account).to.be.instanceof(PublicAccount);
            expect(account.publicKey.toLowerCase()).to.be.equal(defaultPub);
        });

        it('derive second account when given path m/44\'/43\'/1\'/0\'/0\'', () => {
            const xkey = ExtendedKey.createFromSeed(masterSeed, Network.CATAPULT);
            const wallet = new Wallet(xkey);
            const account = wallet.getChildPublicAccount('m/44\'/43\'/1\'/0\'/0\'');

            expect(account).to.be.instanceof(PublicAccount);
            expect(account.publicKey.toLowerCase()).to.be.equal(secondPub);
        });
    });

    describe('Sign & verify', () => {
        it('should sign with private key and verify with public key', () => {

            const text = 'This is a test';
            const masterPriv1 = '28f622172849234508bd5df75c73fba4dc74e466e71eeb01bbefa83a0c977a7a';
            const masterPub1  = 'AA16CB5DE7B47DF9EB805715D871C4AD9D346FA89B5874AA19A7765B681052DD';
            // Sign with master private key
            // const xkey = ExtendedKey.createFromSeed(masterSeed, Network.CATAPULT);
            const xkey = ExtendedKey.createFromSeed(masterSeed, Network.CATAPULT, MACType.KMAC);

            // tslint:disable-next-line:no-console
            console.log('Chain code ' + xkey.node.chainCode.toString('hex'));
            const issuerWallet = new Wallet(xkey);
            const issuerAcccount = issuerWallet.getAccount(NetworkType.PRIVATE_TEST);
             // tslint:disable-next-line:no-console
            console.log('Private Key ' + xkey.getPrivateKey());
            // tslint:disable-next-line:no-console
            console.log('Private Key A- ' + issuerAcccount.privateKey);
            // tslint:disable-next-line:no-console
            console.log('Public Key ' + xkey.getPublicKey());
            // tslint:disable-next-line:no-console
            console.log('Public Key A- ' + issuerAcccount.publicKey);
            const signature  = issuerAcccount.signData(text);

            const pubKey = Buffer.from(masterPub1, 'hex');
            // const myChainCode = Buffer.from(chainCode, 'hex');
            const myChainCode = Buffer.from('fd4af6dcd84c6e0ba28bf402bfffc4b557fb18664640256fe8d6eb68c6bf4e2c', 'hex');
            const keyNode = new NodeEd25519(
                undefined, // privateKey (D part)
                pubKey, // publicKey (Q part)
                myChainCode,
                Network.CATAPULT,
                0, // depth
                0, // index
                0x00000000, // parentFingerprint
            );
            const xKey2 = new ExtendedKey(keyNode, Network.CATAPULT, MACType.KMAC);

            // tslint:disable-next-line:no-console
            console.log('Public Key B- ' + xKey2.getPublicKey());
            const verifierWallet = new Wallet(xKey2);
            expect(verifierWallet.isReadOnly()).to.be.equal(true);
            const verifierAccount = verifierWallet.getPublicAccount(NetworkType.PRIVATE_TEST);
            // tslint:disable-next-line:no-console
            console.log('Public Key AB- ' + verifierAccount.publicKey);
            const isValid = verifierAccount.verifySignature(text, signature);
            expect(isValid).to.be.equal(true);
            /*
            const verifierAccount = PublicAccount.createFromPublicKey(issuerAcccount.publicKey, NetworkType.TEST_NET);
            const isValid = verifierAccount.verifySignature(text, signature);
            expect(isValid).to.be.equal(true);*/

        });
    });
});
