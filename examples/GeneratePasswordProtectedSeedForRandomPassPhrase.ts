import {MnemonicPassPhrase} from 'tsjs-hd-keys';

const mnemonic = MnemonicPassPhrase.createRandom();
const secureSeedHex = mnemonic.toSeed('your-password');
